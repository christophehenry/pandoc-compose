# Minimal installation

## Dependancies

```Bash
$ sudo apt-get install texlive texlive-luatex python-pip3 texmaker pandoc
```

- texlive (or only) texlive-luatex : engines for latex, texlive-luatex si the lualatex engine in texlive
- python-pip3 in order to install pandoc-compose 
- texmaker : as a debugging tools when pdf conversion failed pandoc
- pandoc : necessary to process conversion with meta-data system

## Directory LaTeX class

You need to create `$HOME/texmf/tex/latex`

```Bash
$ mkdir -p $HOME/texmf/tex/latex
```

## Testing

You could see this other [repo](https://git.feneas.org/greenman07/latex-class-example-for-pandoccompose) to download ready-to-use testing files
