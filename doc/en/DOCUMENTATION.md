# Synopsis

```bash
pandoc-compose [-h] [dest] [options]
```
See [CLI options](#cli-options) for options details.

## `pandoc-compose.yml`

This file contains the configuration for batch processing files.
It must be placed at the root of the directory containing the files
you want to process with Pandoc.

### Syntax

The syntax of the file is pure [YAML](https://www.tutorialspoint.com/yaml/index.htm).
Here is a config example:

```yaml
files:
  - "*.md": latex
pandoc:
  author:
  - Karl Marx
  documentclass: report
  toc: False

```

#### `files` section

In this section, you indicate which type of files you want to process
and to which format convert them. It's a list of dictionnary where
the key a regex to find the files and the value is the target format.

The target format is [whatever format Pandoc is able to convert to](https://pandoc.org/MANUAL.html#specifying-formats).

The key is a simple regex against which every file in the directory
will be tested. Matching files will be converted to the target format.
The regex matches Python's [`fnmatch`](https://docs.python.org/3.5/library/fnmatch.html#fnmatch.fnmatch)
pattern matching.

For instance, if you want to convert your Markdown files to PDF, provided
those Markdown files ends with extension `.md`, you will write:

```yaml
files:
  - "*.md": pdf
```

Now, suppose you have multiple Markdown files in your directory, some
which you want to convert to PDF, some of which you want to convert to
HTML. Then, you can write:

```yaml
files:
  - "*-pdf.md": pdf
  - "*-html.md": html
```

Or, you can also write:

```yaml
files:
  - "pdf/*.md": pdf
  - "html/*.md": html
```

and place files to convert to PDF in the `pdf` subdirectory and files
to be converted to HTML in the `html` subdirectory.

Files will be converted according to the first matching pattern found.

Note that the configuration above will ignore every other file than
those found directly at the root of `pdf` and `html` subdirectories.

If you want a generic convertion of every other Markdown file in the
processed directory, you may want to use the below configuration:

```yaml
files:
  - "pdf/*.md": pdf
  - "html/*.md": html
  - "**/*.md": pdf
```

If `files` section is absent from `pandoc-compose.yml`, the following
configuration is implied:

```yaml
files:
  - "**/*.md": pdf
```

**Limitations**:
  - Symbolic links won't be processed
  - Not every output format is supported

#### `pandoc` section

In this section, you can put any [Pandoc template variable](https://pandoc.org/MANUAL.html#templates).

### `pandoc_options` section

You can pass any additionnal [Pandoc option](https://pandoc.org/MANUAL.html#options)
in this section. For instance:

```yaml
pandoc_options:
  - --pdf-engine=lualatex
  - --verbose
```

### `pandoc_compose_options` section

See [CLI options](#cli-options)

## Markdown metadata

[Pandoc lets you define metadata in Markdown files](https://pandoc.org/MANUAL.html#metadata-variables).
You can use this section to also override some of the behaviours
of `pandoc-compose` in a `pandoc_compose` section.


### Usage

Basically, you can override some sections defined
in `pandoc-compose.yml`. Not every section, though. For instance,
since the [`pandoc`](#pandoc-section) already corresponds to options
you can define in this metadata bloc, you won't be able to override it
here, so this won't work:

```yaml
---
author:
- Aristotle
- Peter Abelard
pandoc_compose:
    pandoc:
        author:
        - Karl Marx
...
```

The `pandoc_compose.pandoc` subsection will just be ignored.

### Possible overridable sections

You can override the following sections of `pandoc-compose.yml`:

*  [`pandoc_options`](#pandoc_options-section)

### Example

Let's say you have the following `pandoc-compose.yml` file:

```yaml
files:
  - "*.md": latex
pandoc:
  author:
  - Karl Marx
  documentclass: report
  toc: False
pandoc_options:
  - --pdf-engine=lualatex
  - --verbose
```

You use `lualatex` as your default engine for every markdown file. But,
for some reason, you want to use `pdflatex` for one particular MD file.

Then, you can put the following in the header:
```yaml
---
author:
- Aristotle
- Peter Abelard
pandoc_compose:
    pandoc_options:
    - --pdf-engine=pdflatex
...

# My mardown file
```

## CLI options

`pandoc-compose` features a few options. All options can be set either
directly as CLI options or in `pandoc-compose.yml`. In `pandoc-compose.yml`,
the key always corresponds to the long version of the option where dashes are
replaced by underscores. For instance, if the option is `-o` with long version
`--output-directory`, the corresponding configuration in `pandoc-compose.yml`
will be:

```yaml
---
author:
- Aristotle
- Peter Abelard

pandoc_compose_options:
    ouput_directory: ...
...
```

Options used in command line always take precedence over options set in
`pandoc-compose.yml`.

Here is the full list of options :

|            CLI option            |                                                                                                        Description                                                                                                         |
|:--------------------------------:|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|         `-f`, `--force`          |                    Enables the force mode. All files will be converted whther they have changed or not. In `pandoc_compose_options`, possible values are `yes`, \`no`, `true`, `false, `True`, `False`                     |
| `-v`, `-vv`, `-vvv`, `--verbose` |               Verbose mode. `-v` and `--verbose` enables warning logs, `-vv` enables info logs and `-vvv` enables debug logs. In `pandoc_compose_options`, possible values are `WARNING`, `INFO` and `DEBUG`               |
|         `t`, `--timeout`         | Set a timeout after which, if a file wasn't converted, its conversion is aborted. Possible values are a integer of second or a human readable interval in hours, minutes or seconds. Examples : `1000`, `10s`, `5m`, `6h`. |
|               `--`               |                                                                  Extra arguments that will be passed to Pandoc[¹]. For instance: `-- --pdf-engine=latex`                                                                   |

[¹]: Everything coming after `--` will be ignored by `pandoc-compose` and
directly passed to `pandoc` as is. There is no corresponding key in
`pandoc_compose_options`. Everything coming after `--` can also be put in the
[`pandoc_options` section](#pandoc_options-section). Like other CLI options,
every Pandoc option passed after `--` will take precedence over Pandoc options
set in `pandoc_options` section. Also, since every Pandoc option, either passed
after `--` or in `pandoc_options` section will take precedence over every other
`pandoc-compose` configuration. As such, this option is some form of nuclear
bomb that should only be used when there is absolutely no other solution.
