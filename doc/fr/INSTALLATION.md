# Installation minimale

## Dependences

```Bash
$ sudo apt-get install texlive texlive-luatex python-pip3 texmaker pandoc
```

- texlive (or only) texlive-luatex : les moteurs pour LaTeX, texlive-luatex est la version du moteur lualatex pour texlive
- python-pip3 pour installer pandoc-compose 
- texmaker : en tant qu'outil de débogage LaTeX, quand les conversions échouent suite à une modification du script
- pandoc : est nécessaire pour apporter la gestion des méta-données et la conversion de format non pris en charge par LaTeX

## Répertoire des classes LaTeX

IL va falloir créer `$HOME/texmf/tex/latex`, pour cela :

```Bash
$ mkdir -p $HOME/texmf/tex/latex
```

## Testing

Vous pouvez récupérer sur cet autre [dépôt](https://git.feneas.org/greenman07/latex-class-example-for-pandoccompose) des fichiers de configurations basiques à déployer pour tester votre nouvelle installation.
