# Synopsis

```bash
pandoc-compose [-h] [dest] [options]
```
Voir les options de la ligne de commande [(Cli-options)](#cli-options) pour plus de détails sur les options.

# Français

## `pandoc-compose.yml`

Ce fichier contient la configuration pour l'automatisation de la production de document formaté.
Il doit être placé à la racine du répertoire qui contient les fichiers sources que vous souhaitez transformer avec pandoc.

### Syntaxe

La syntaxe de ce fichier est du pur [YAML](https://www.tutorialspoint.com/yaml/index.htm).
Voici un exemple de configuration :  

```yaml
files:
  - "*.md": latex
pandoc:
  author:
  - Karl Marx
  documentclass: report
  toc: False

```

### Les sections

#### `files`

Dans cette section, vous pouvez indiquer chaque type de fichier que vous souhaitez voir converti et dans quel format d'arrivée vous souhaitez les voir produits.
La liste provient d'un dictionnaire et il est possible d'utiliser des RegExp pour détecter les fichiers à convertir ainsi que les différents types de format final.

[Tous les formats de sorties supportés par pandoc](https://pandoc.org/MANUAL.html#specifying-formats) sont utilisables pour produire les documents.

La clé est une expression régulière simple qui va tester tous les fichiers du répertoire. Les fichiers validant cette RegExp seront alors converti par pandoc à l'exécution de pandoc-compose.
Les expressions régulières utilisables sont celles permises par la libraire python [`fnmatch`](https://docs.python.org/3.5/library/fnmatch.html#fnmatch.fnmatch)

Ainsi, pour un répertoire donné contenant des fichiers .md (MarkDown) à convertir en pdf, la section `files` sera de la forme :

```yaml
files:
  - "*.md": pdf
```

Si une part des fichiers doit être convertie en html et l'autre en pdf, il est possible d'utiliser une convention de nommage des fichiers sources, couplé aux expressions régulières :

```yaml
files:
  - "*-pdf.md": pdf
  - "*-html.md": html
```

Cette configuration permet donc de transcrire tous les fichiers du répertoire terminant par -pdf.md en pdf et et ceux terminant par -html.md en présentation html.



Une autre manière de faire serait de gérer les fichiers par sous-répertoire, en créant un sous-répertoire par type de format de destination, les expressions régulières prendraient alors la forme suivante (pour des sous-répertoires pdf et html contenant les fichiers à convertir vers ces deux formats respectivement) :

```yaml
files:
  - "pdf/*.md": pdf
  - "html/*.md": html
```

Note : Cette configuration ignore donc le répertoire racine, aucune expression régulière ne pouvant être satisfaite pour ce dernier.

Important : l'évaluation des expressions régulières se faisant de haut en bas, la première règle qu'un fichier satisfait entraîne la conversion du fichier.


Enfin, nos expressions régulières permettent :

```yaml
files:
  - "pdf/*.md": pdf
  - "html/*.md": html
  - "**/*.md": pdf
```

Dans le cas où aucune section `file` ne serait indiquée dans le yaml, la configuration par défaut suivante s'applique :
```yaml
files:
  - "**/*.md": pdf
```

**Limitations**:
  - Les liens symboliques ne seront pas suivis
  - Tous les formats d'export de pandoc ne sont pas supportés pour l'instant

#### La section `pandoc`

Dans cette section, vous pouvez utiliser n'importe quel modèle de variable prévu pour Pandoc. La liste est complète est disponible dans la documentation du projet (en anglais) : [Pandoc template variable](https://pandoc.org/MANUAL.html#templates).

### La section `pandoc_options`

Dans cette section, vous pouvez passer à Pandoc n'importe quelle option existante pour celui-ci. La liste de ces options est disponible sur la page de documentation [des options de Pandoc](https://pandoc.org/MANUAL.html#options)
Par exemple :

```yaml
pandoc_options:
  - --pdf-engine=lualatex
  - --verbose
```

### La section `pandoc_compose_options`

Voir les options possibles dans [CLI options](#cli-options)

## Les méta-données Markdown

Pandoc permet de définir des variables de méta-données pour MarkDown. [Voir la doc officielle en anglais](https://pandoc.org/MANUAL.html#metadata-variables).


### Usage

Vous pouvez donc utiliser un en-tête Yaml directement dans vos fichiers MarkDown, ces méta-données écraseront alors les valeurs fixées pour `pandoc-compose` via la section `pandoc_compose` du fichier `pandoc-compose.yml`

Pas toutes les sections, cependant. Pour le moment, si la section [`pandoc`](#la-section-pandoc) contient une option déjà définie auparavant, vous ne pourrez pas écraser la valeur de cette manière.  
L'exemple suivant ne pourrait donc pas marcher  :

```yaml
---
author:
- Aristotle
- Peter Abelard
pandoc_compose:
    pandoc:
        author:
        - Karl Marx
...
```

Dans ce cas, la sous-section `pandoc_compose.pandoc` sera simplement ignorée et les auteurs resteront ceux définis dans `author:`.

### Sections pouvant être remplacées

Les sections `pandoc-compose.yml` que vous pouvez remplacer de cette manière sont actuellement :

*  [`pandoc_options`](#la-section-pandoc_options)

### Exemple

Imaginons que nous avons le fichier `pandoc-compose.yml` suivant :

```yaml
files:
  - "*.md": latex
pandoc:
  author:
  - Karl Marx
  documentclass: report
  toc: False
pandoc_options:
  - --pdf-engine=lualatex
  - --verbose
```

Nous souhaitons utiliser `lualatex` comme moteur par défaut pour la conversion des fichiers MarkDown. Mais, pour une raison ou une autre, on préfèrera utiliser plutôt `pdflatex` pour un fichier MarkDown particulier.

Pour cela, on va mettre un en-tête spécifique directement dans le fichier MarkDown concerné :  

```yaml
---
author:
- Aristotle
- Peter Abelard
pandoc_compose:
    pandoc_options:
    - --pdf-engine=pdflatex
...

# My mardown file
```

## CLI options

`pandoc-compose` est fourni avec quelques options basiques. Toutes ces options peuvent être définies directement en appel via la ligne de commande ou dans le fichier `pandoc-compose.yml`.  
Dans le fichier `pandoc-compose.yml`, les options sont toujours à utiliser en format long et en remplaçant les tirets (tirets du 6) par des blancs soulignés (tirets du 8).  
Par exemple, si l'option que l'on veut utiliser est `-o` dont la version longue est `--output-directory`, nous la définirons avec l'orthographe suivante via le fichier `pandoc-compose.yml` :   

```yaml
---
author:
- Aristotle
- Peter Abelard

pandoc_compose_options:
    ouput_directory: ...
...
```

Une option passée directement en ligne de commande prévaudra toujours sur celle définie dans le fichier `pandoc-compose.yml`.

Voici la liste complète des options disponibles :

| Options de la ligne de commandes | Description |
|:--------------------------------:|:----------------------:|
| `-f`, `--force` | Active le mode force. Tous les fichiers seront convertis qu'ils aient changé ou non et seront reproduits. Dans la `pandoc_compose_options`, les valeurs possibles sont `yes`, \`no`, `true`, `false, `True`, `False`|
| `-v`, `-vv`, `-vvv`, `--verbose` | Active le mode verbeux. `-v` et `--verbose` active les messages de premier niveau (Warnings), `-vv` active en plus un second niveau (Info) et `-vvv` active les messages de débogage (Debug). Dans la section `pandoc_compose_options`, les valeurs correspondantes seront `WARNING`, `INFO` et `DEBUG`|
| `t`, `--timeout` | Permet de définir un temps limite après lequel pandoc-compose arrêtera les conversions, si une conversion est en toujours en compte à la fin de ce délai, elle sera abandonné. Par défaut, ce temps est exprimé en seconde et doit être un nombre entier. Il est également possible de préciser secondes, minutes ou heure en accolant la lettre correspondant au nombre entier, respectivement `s`, `m` ou `h`. Exemples : `1000`, `10s`, `5m`, `6h`. |
|`--`|Le double-tiret précède les arguments qui seront passés directement à Pandoc[¹] et seront interprêtés par lui. Par exemple: `-- --pdf-engine=latex`|

[¹]: Tout ce qui vient après ces doubles tirets `--` sera ignoré par `pandoc-compose` et sera passé à `pandoc` tel quel. Il n'y a aucun correpondance possible avec les options `pandoc_compose_options`. Tout ce qui vient après ces `--` peut également être défini dans [la section `pandoc_options`](#la-section-pandoc_options) du fichier yaml. Comme pour les autres options de ligne de commandes, toutes options passées en ligne de commande après `--` prévaudra sur les options définies dans la section `pandoc_options` du fichier yaml. Toutes options adressées à Pandoc, que ce soit par le biais des `--` ou via la section `pandoc_options` prévaudra sur toutes options définies pour `pandoc-compose`. Ainsi, cette possibilité de passer des options directement à pandoc, en ligne de commande est une sorte de bombe nucléaire qui prendra le pas sur toutes les autres.
